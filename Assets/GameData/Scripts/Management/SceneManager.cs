﻿using System.Collections;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public void LoadLevelAsync(string sceneName)
    {
        StartCoroutine(LoadAsyncScene(sceneName));
    }

    public void LoadScene(string SceneName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(SceneName);
    }

    public void LoadAdditiveScene(string SceneName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(SceneName, UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }

    IEnumerator LoadAsyncScene(string sceneName)
    {

        AsyncOperation asyncLoad = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
