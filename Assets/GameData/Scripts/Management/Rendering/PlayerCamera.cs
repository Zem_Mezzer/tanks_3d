﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
public class PlayerCamera : MonoBehaviour
{
    private void Start()
    {
        SetGraphicsSettings();
    }

    void SetGraphicsSettings()
    {
        Settings settings = JsonUtility.FromJson<Settings>(System.IO.File.ReadAllText(WebManager.Instance.streamingAssetsPath + "/Settings/Settings.json"));
        Debug.Log(settings.qualityLevel);
        switch (settings.qualityLevel)
        {
            case "Low":
                QualitySettings.SetQualityLevel(1);
                break;
            case "Medium":
                QualitySettings.SetQualityLevel(2);
                break;
            case "High":
                QualitySettings.SetQualityLevel(3);
                break;
            case "Ultra":
                QualitySettings.SetQualityLevel(5);
                break;
        }

        var postProcessing = GetComponent<PostProcessLayer>();

        postProcessing.enabled = settings.postProcessing;
        if (settings.antiAliasing)
        { postProcessing.antialiasingMode = PostProcessLayer.Antialiasing.FastApproximateAntialiasing; }
        else
        { postProcessing.antialiasingMode = PostProcessLayer.Antialiasing.None; }
    }
}
