﻿using System.IO;
using UnityEngine;

public class HostUIManager : MonoBehaviour
{
    [SerializeField] Transform buttonsHandler;
    [SerializeField] GameObject button;
    public AudioClip clickSound;

    private void Start()
    {
        string[] levels = FileManager.Instance.GetFiles("Levels");

        for(int i = 0; i < levels.Length; i++)
        {
            var _button = Instantiate(button, buttonsHandler);
            _button.GetComponent<LevelButton>().levelName = Path.GetFileNameWithoutExtension(levels[i]);
        }
    }

    public void BackButton()
    {
        PlayClickSound();
        SceneManager sceneManager = new SceneManager();
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Host");
    }

    void PlayClickSound()
    {
        SoundManager.Instance.PlaySound(clickSound);
    }
}
