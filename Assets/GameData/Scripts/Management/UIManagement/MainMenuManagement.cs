﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManagement : MonoBehaviour
{
    public AudioClip clickSound;
    public void HostGame()
    {
        PlayClickSound();
        SceneManager sceneManager = new SceneManager();
        sceneManager.LoadAdditiveScene("Host");
    }

    public void Connect()
    {
        PlayClickSound();
        SceneManager sceneManager = new SceneManager();
        sceneManager.LoadAdditiveScene("Connect");
    }

    public void Settings()
    {
        PlayClickSound();
        SceneManager sceneManager = new SceneManager();
        sceneManager.LoadAdditiveScene("Settings");
    }

    public void Exit()
    {
        PlayClickSound();
        Application.Quit();
    }

    public void PlayClickSound()
    {
        SoundManager.Instance.PlaySound(clickSound);
    }
}
