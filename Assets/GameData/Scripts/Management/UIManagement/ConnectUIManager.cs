﻿using UnityEngine;

public class ConnectUIManager : MonoBehaviour
{
    public UnityEngine.UI.InputField addressField;
    public AudioClip clickSound;
    public void Connect()
    {
        PlayClickSound();
        WebManager.Instance.ConnectGame(addressField.text);
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Connect");
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Menu");
    }

    public void Back()
    {
        PlayClickSound();
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Connect");
    }

    void PlayClickSound()
    {
        SoundManager.Instance.PlaySound(clickSound);
    }
}
