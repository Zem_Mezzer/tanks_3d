﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    public Toggle aAToggle;
    public Toggle pPToggle;

    string filePath = WebManager.Instance.streamingAssetsPath + "/Settings/Settings.json";

    Settings settings = new Settings();

    public AudioClip clickSound;

    private void Start()
    {
        settings = JsonUtility.FromJson<Settings>(File.ReadAllText(filePath));
        aAToggle.isOn = settings.antiAliasing;
        pPToggle.isOn = settings.postProcessing;
    }

    public void SetSettings(string settingsLevel)
    {
        PlayClickSound();
        settings.qualityLevel = settingsLevel;
    }

    public void Close()
    {
        PlayClickSound();
        settings.antiAliasing = aAToggle.isOn;
        settings.postProcessing = pPToggle.isOn;

        string json = JsonUtility.ToJson(settings);
        File.WriteAllText(filePath, json);
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Settings");
    }

    void PlayClickSound()
    {
        SoundManager.Instance.PlaySound(clickSound);
    }
}

[System.Serializable]
public class Settings
{
    public string qualityLevel;
    public bool antiAliasing;
    public bool postProcessing;
}
