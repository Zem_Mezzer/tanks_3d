﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuUIManager : MonoBehaviour
{
    public AudioClip clickSound;

    public void Close()
    {
        PlayClickSound();
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("GameMenu");
    }
    public void Exit()
    {
        PlayClickSound();
        WebManager.Instance.Disconnect();
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("GameMenu");
        SceneManager sceneManager = new SceneManager();
        sceneManager.LoadAdditiveScene("Menu");
    }

    void PlayClickSound()
    {
        SoundManager.Instance.PlaySound(clickSound);
    }
}
