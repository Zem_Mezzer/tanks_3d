﻿using UnityEngine;
using Mirror;

public class WebUIManager : NetworkBehaviour
{
    private void Update()
    {
        if (NetworkServer.active || NetworkClient.isConnected)
        {
            if (Input.GetButtonDown("OpenMenu"))
            {
                OpenGameMenu();
            }
        }
    }

    [Client]
    public void OpenGameMenu()
    {

            if (!UnityEngine.SceneManagement.SceneManager.GetSceneByName("GameMenu").isLoaded)
            {
                SceneManager sceneManager = new SceneManager();
                sceneManager.LoadAdditiveScene("GameMenu");
            }
            else
            {
                UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("GameMenu");
            }

        
    }
}
