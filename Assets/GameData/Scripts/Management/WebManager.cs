﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Better.StreamingAssets;
using System.IO;

public class WebManager : MonoBehaviour
{
    public ServerManager networkManager;
    public string streamingAssetsPath;

    public static WebManager Instance;

    void Start()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //if (!NetworkClient.active) { return; }
        BetterStreamingAssets.Initialize();
        GameInitialize();
    }

    void GameInitialize()
    {
        streamingAssetsPath = BetterStreamingAssets.Root;

        string filePath = streamingAssetsPath + "/Settings/Settings.json";
        Debug.Log(filePath);
        if (!File.Exists(filePath))
        {
            File.Create(filePath).Close();
            Settings settings = new Settings();
            settings.antiAliasing = true;
            settings.postProcessing = true;
            settings.qualityLevel = "Ultra";
            string json = JsonUtility.ToJson(settings);
            File.WriteAllText(filePath, json);
        }
    }

    public void HostGame(string sceneName)
    {
        RegisterPrefabs();
        networkManager.InitializeHost(sceneName);
    }


    public void ConnectGame(string address)
    {
        RegisterPrefabs();
        networkManager.StartClient();
        networkManager.networkAddress = address;
    }


    void RegisterPrefabs()
    {
        for (int i = 0; i < networkManager.objectsForRegister.Count; i++)
        {
            ClientScene.RegisterPrefab(networkManager.objectsForRegister[i]);
        }
    }
    

    public void Disconnect()
    {
        if (NetworkServer.active)
        {
            networkManager.StopHost();
        }
        else if(NetworkClient.isConnected)
        {
            networkManager.StopClient();
        }
    }
}
