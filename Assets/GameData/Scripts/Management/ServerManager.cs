﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerManager : NetworkManager
{

    public static ServerManager Instance;
    [SerializeField] WebManager webManager;

    [Header("Objects for register")]
    [SerializeField] GameObject[] effects;
    [SerializeField] GameObject[] worldBlocks;
    [SerializeField] GameObject[] collectableObjects;
    [SerializeField] GameObject[] bullets;
    [SerializeField] GameObject[] environmentObjects;

    [HideInInspector] public List<GameObject> objectsForRegister = new List<GameObject>();

    public override void Awake()
    {
        objectsForRegister.AddRange(effects);
        objectsForRegister.AddRange(worldBlocks);
        objectsForRegister.AddRange(collectableObjects);
        objectsForRegister.AddRange(bullets);
        objectsForRegister.AddRange(environmentObjects);

        base.Awake();
    }

    public void InitializeHost(string sceneName)
    {
        StartHost();
        string json = FileManager.Instance.GetText("Levels/" + sceneName + ".json");
        LevelModel level = JsonUtility.FromJson<LevelModel>(json);
        for (int i = 0; i < level.entities.Count; i++)
        {
            for (int j = 0; j < objectsForRegister.Count; j++)
            {
                if (objectsForRegister[j].name == level.entities[i].name)
                {
                    var obj = Instantiate(objectsForRegister[j], level.entities[i].position, level.entities[i].rotation);
                    obj.transform.localScale = level.entities[i].scale;
                    NetworkServer.Spawn(obj);
                }
            }
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        SceneManager sceneManager = new SceneManager();
        sceneManager.LoadAdditiveScene("Menu");

        base.OnClientDisconnect(conn);
    }


    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Transform playerStart = null;
        switch (numPlayers)
        {
            case 0:
                playerStart = GameObject.FindGameObjectWithTag("Spawn Point 1").transform;
                break;
            case 1:
                playerStart = GameObject.FindGameObjectWithTag("Spawn Point 2").transform;
                break;
            case 2:
                playerStart = GameObject.FindGameObjectWithTag("Spawn Point 3").transform;
                break;
            case 3:
                playerStart = GameObject.FindGameObjectWithTag("Spawn Point 4").transform;
                break;
        }
        var player = Instantiate(playerPrefab, playerStart.position, playerStart.rotation);
        NetworkServer.AddPlayerForConnection(conn, player);
    }
}
