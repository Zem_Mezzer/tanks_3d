﻿using UnityEngine;

public class FileManager : MonoBehaviour
{
    public static FileManager Instance;

    public void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            BetterStreamingAssets.Initialize();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public string GetText(string path)
    {
        return BetterStreamingAssets.ReadAllText("/"+path);
    }


    public string[] GetFiles(string path)
    {
        return BetterStreamingAssets.GetFiles("/"+path, "*.json", System.IO.SearchOption.TopDirectoryOnly);
    }

}

