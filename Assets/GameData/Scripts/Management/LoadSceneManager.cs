﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSceneManager : MonoBehaviour
{
    public string loadingSceneName;


    private void Update()
    {
        if (loadingSceneName != "")
        {
            Debug.Log(loadingSceneName);
            SceneManager sceneManager = new SceneManager();
            sceneManager.LoadAdditiveScene(loadingSceneName);
            loadingSceneName = "";
        }
    }
}
