﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class LevelEditor : MonoBehaviour
{
#if UNITY_EDITOR
    public List<GameObject> objectsDatabase;

    public string fileName;

    [ContextMenu("Save In Json")]
    public void SaveInJson()
    {
        LevelModel levelModel = new LevelModel();

        for (int i = 0; i < transform.childCount; i++)
        {
            NetworkEntity entity = new NetworkEntity();


            var prefabOfGameObject = PrefabUtility.GetCorrespondingObjectFromSource(transform.GetChild(i));
            entity.name = prefabOfGameObject.name;
            entity.position = transform.GetChild(i).position;
            entity.rotation = transform.GetChild(i).rotation;
            entity.scale = transform.GetChild(i).localScale;
            levelModel.entities.Add(entity);
        }
        string json = JsonUtility.ToJson(levelModel);
        string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        Debug.Log("File saved in: "+"Assets/StreamingAssets/Levels");
        if (!Directory.Exists("Assets/StreamingAssets/Levels"))
        {
            Directory.CreateDirectory("Assets/StreamingAssets/Levels");
        }

        File.Create("Assets/StreamingAssets/Levels/" + fileName +".json").Close();
        File.WriteAllText("Assets/StreamingAssets/Levels/" + fileName + ".json", json);
    }

    [ContextMenu("Load From Json")]
    public void LoadFromJson()
    {

        LevelModel levelModel = JsonUtility.FromJson<LevelModel>(File.ReadAllText("Assets/StreamingAssets/Levels/"+fileName+".json"));

        for(int  i = 0; i < levelModel.entities.Count; i++)
        {
            for(int j = 0; j < objectsDatabase.Count; j++)
            {
                if(levelModel.entities[i].name == objectsDatabase[j].name)
                {
                    var obj = PrefabUtility.InstantiatePrefab(objectsDatabase[j],transform);

                    ((GameObject)obj).transform.position = levelModel.entities[i].position;
                    ((GameObject)obj).transform.rotation = levelModel.entities[i].rotation;
                    ((GameObject)obj).transform.localScale = levelModel.entities[i].scale;
                }
            }
        }
    }
#endif
}
