﻿using UnityEngine;
using System;

[Serializable]
public class NetworkEntity
{
    public string name;
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
}
