﻿using System.Collections.Generic;
using System;

[Serializable]
public class LevelModel
{
    public List<NetworkEntity> entities = new List<NetworkEntity>();
}
