﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelButton : MonoBehaviour
{
    public string levelName;
    [SerializeField] UnityEngine.UI.Text buttonText;
    public AudioClip clickSound;

    // Start is called before the first frame update
    void Start()
    {
        buttonText.text = levelName;
    }

    public void StartLevel()
    {
        WebManager.Instance.HostGame(levelName);
        SoundManager.Instance.PlaySound(clickSound);
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Host");
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Menu");
    }
}
