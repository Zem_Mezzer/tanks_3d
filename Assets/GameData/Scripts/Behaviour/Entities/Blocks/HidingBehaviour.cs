﻿using UnityEngine;
using Mirror;

public class HidingBehaviour : NetworkBehaviour
{
    [SerializeField] ParticleSystem interactEffect;
    private void OnTriggerEnter(Collider other)
    {
        interactEffect.Play();
    }
}
