using UnityEngine;
using Mirror;

public class BonusBox : NetworkBehaviour
{
    [SerializeField] Weapon weapon;
    [SerializeField] AudioClip pickUpSound;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerBehaviour>().currentWeapon = weapon;
            NetworkServer.Destroy(gameObject);
            PlaySound();
        }
    }
    [Client]
    private void PlaySound()
    {
        SoundManager.Instance.PlaySound(pickUpSound);
    }
}
