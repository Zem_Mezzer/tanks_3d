using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BonusSpawner : NetworkBehaviour
{
    float spawnTimer;
    GameObject bonus;
    [SerializeField] new Light light;
    [SerializeField] GameObject[] bonuses;
    [SerializeField] float[] times;

    void Start()
    {
        Initialize();
    }

    void Initialize()
    {
        spawnTimer = times[Random.Range(0, times.Length-1)];
    }

    void Update()
    {
        if (bonus == null)
        {
            light.color = Color.red;
            spawnTimer -= Time.deltaTime * 1;
            if (spawnTimer <= 0)
            {
                SpawnBonus();
            }
        }
        else
        {
            light.color = Color.clear;
        }
    }

    [Server]
    void SpawnBonus()
    {
        bonus = Instantiate(bonuses[Random.Range(0, bonuses.Length - 1)], new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z), transform.rotation);
        NetworkServer.Spawn(bonus);
        Initialize();
    }
}
