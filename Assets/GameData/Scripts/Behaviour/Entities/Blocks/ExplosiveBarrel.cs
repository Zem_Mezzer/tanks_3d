﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBarrel : Block
{
    [SerializeField] float explosionRange;
    [SerializeField] float explosionDamage;

    private Transform startPos;

    public override void Start()
    {
        base.Start();
        GameObject go = new GameObject();
        go.transform.position = transform.position;
        go.transform.rotation = transform.rotation;
        startPos = go.transform;
    }

    [Mirror.ClientRpc]
    public override void GetDamage(float damage)
    {
        base.GetDamage(damage);
        if (hp <= 0)
        {
            Collider[] objects = Physics.OverlapSphere(transform.position, explosionRange);
            Collider[] hit = Physics.OverlapSphere(transform.position, explosionRange);
            transform.position = startPos.position;
            transform.rotation = startPos.rotation;

            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i].gameObject.GetComponent<IEntity>() != null && objects[i].transform.root !=transform)
                {
                    objects[i].gameObject.GetComponent<IEntity>().GetDamage(explosionDamage);
                }
            }
            
            for(int i = 0; i < hit.Length; i++)
            {
                if (hit[i].gameObject.GetComponent<Rigidbody>() != null && objects[i].transform.root != transform)
                {
                    hit[i].gameObject.GetComponent<Rigidbody>().AddExplosionForce(300,transform.position,100,10);
                }
            }
        }
    }

    private void OnDestroy()
    {
        try
        {
            Destroy(startPos.gameObject);
        }
        catch
        {

        }
    }
}
