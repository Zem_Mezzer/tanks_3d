﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Block : NetworkBehaviour,IEntity
{
    [SerializeField] float _hp;
    protected float hp;
    [SerializeField] GameObject destroyParticles;
    [SerializeField] AudioClip destroySound;
    [SerializeField] bool isDestrDecoration;

    protected bool isDestroed = false;
    float destroyTimer = 10;

    public virtual void Start()
    {
        hp = _hp;
    }

    [ClientRpc]
    public virtual void GetDamage(float damage)
    {
        hp -= damage;
        if (hp <= 0)
        {

            var destroyEffect = Instantiate(destroyParticles, transform.position, transform.rotation);
            NetworkServer.Spawn(destroyEffect);
            GetComponent<MeshRenderer>().enabled = !enabled;
            GetComponent<BoxCollider>().enabled = false;
            try { GetComponent<Rigidbody>().isKinematic = true; } catch { }
            SoundManager.Instance.PlaySound(destroySound);
            isDestroed = true;
        }
    }

    void Update()
    {
        if (isDestroed)
        {
            destroyTimer -= 1 * Time.deltaTime;
            if (destroyTimer <= 0)
            {
                Collider[] collider = Physics.OverlapBox(transform.position,transform.localScale);
                bool canRespawn = true;

                for(int i = 0; i < collider.Length; i++)
                {
                    if(collider[i].gameObject.tag == "Player")
                    {
                        canRespawn = false;
                    }
                }
                if (canRespawn)
                {
                    GetComponent<MeshRenderer>().enabled = enabled;
                    GetComponent<BoxCollider>().enabled = true;
                    try { GetComponent<Rigidbody>().isKinematic = false; } catch { }
                    destroyTimer = 10;
                    hp = _hp;
                    isDestroed = false;
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isDestrDecoration)
        {
            GetDamage(1000);
        }
    }
}
