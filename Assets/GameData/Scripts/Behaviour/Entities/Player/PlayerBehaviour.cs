﻿using UnityEngine;
using Mirror;
public class PlayerBehaviour : NetworkBehaviour,IEntity
{
    [Header("Stats")]
    [SerializeField] Weapon defaultWeapon;
     public Weapon currentWeapon;
    [SerializeField] float hp;
    private float _hp;
    [SerializeField] float playerSpeed;

    [Header("Effects")]
    [SerializeField] Transform firePoint;
    [SerializeField] GameObject snowParticles;
    [SerializeField] GameObject explosion;

    [Header("Sounds")]
    [SerializeField] AudioClip moveSound;
    [SerializeField] AudioClip deathSound;

    [Header("Links")]
    [SerializeField] new GameObject camera;
    private new AudioSource audio;
    private new Rigidbody rigidbody;
    private Animator animator;

    [Header("Death logic")]
    [SerializeField] GameObject body;
    [SerializeField] GameObject deadBody;
    bool isDead = false;
    float respawnDalay = 5;
    Transform respawnTransform;




    // Start is called before the first frame update
    void Start()
    {
        PlayerInitialization();
    }


    
    private void FixedUpdate()
    {
        if (isLocalPlayer && !isDead)
        {
            Move();
            Animate();
        }
    }


    float fireDalay = 0;
    void Update()
    {
        fireDalay -= 1 * Time.deltaTime;

        if (isLocalPlayer && !isDead)
        {
            Shoot();
        }

        if (isDead)
        {
            respawnDalay -= 1 * Time.deltaTime;
            if (respawnDalay <= 0)
            {
                Respawn();
            }
        }
    }

    

    private void OnDestroy()
    {
        try
        {
            Destroy(camera);
            Destroy(respawnTransform.gameObject);
        }
        catch
        {

        }
    }

    void Animate()
    {
        float hSpeed = Input.GetAxisRaw("Horizontal");
        float vSpeed = Input.GetAxisRaw("Vertical");

        animator.SetFloat("Speed", Mathf.Abs(hSpeed) + Mathf.Abs(vSpeed));
        if (Mathf.Abs(hSpeed) > 0 || Mathf.Abs(vSpeed) > 0)
        {
            snowParticles.SetActive(true);
        }
        else
        {
            snowParticles.SetActive(false);
        }
    }

    void Move()
    {
        float hSpeed = Input.GetAxisRaw("Horizontal");
        float vSpeed = Input.GetAxisRaw("Vertical");
        rigidbody.velocity = new Vector3(hSpeed * playerSpeed, rigidbody.velocity.y, vSpeed * playerSpeed);
        
        if (Mathf.Abs(hSpeed) > 0 || Mathf.Abs(vSpeed) > 0)
        {
            Rotate(hSpeed, vSpeed);
            audio.volume = 1;
        }
        else
        {
            audio.volume = 0;
        }
    }

    void Rotate(float x, float y)
    {
        float Angle = Mathf.Atan2(x, y);
        Angle = Mathf.Rad2Deg * Angle;
        Quaternion targetRotation = Quaternion.Euler(0, Angle, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 0.2f);
    }

    #region WebLogic

    public void Shoot()
    {
        if (Input.GetButtonDown("Fire") && fireDalay <= 0 &&isLocalPlayer)
        {
            PlayerSkills();
            if (currentWeapon.shootSound != null)
            {
                SoundManager.Instance.PlaySound(currentWeapon?.shootSound);
            }
            fireDalay = currentWeapon.rechargeTime;
        }
    }

    [Command]
    void PlayerSkills()
    {
        var _bullet = Instantiate(currentWeapon.bulletPrefab, firePoint.position, transform.rotation);
        if (currentWeapon.fireEffect != null)
        {
            var shootEffect = Instantiate(currentWeapon.fireEffect, firePoint.position, firePoint.rotation);
            NetworkServer.Spawn(shootEffect);
        }
        NetworkServer.Spawn(_bullet);
    }

    [Command]
    void SpawnDeadBody()
    {
        var _deadBody = Instantiate(deadBody, transform.position, transform.rotation);
        NetworkServer.Spawn(_deadBody);
        _deadBody.GetComponent<Rigidbody>().AddExplosionForce(300, transform.position, 100, 10);
    }

    [ClientRpc]
    public void GetDamage(float damage)
    {
        _hp -= damage;
        if (_hp <= 0)
        {
            Death();
        }
    }

    [ClientRpc]
    void Death()
    {
        if (!isDead)
        {
            isDead = true;
            GetComponent<MeshRenderer>().enabled = !enabled;
            GetComponent<BoxCollider>().enabled = !enabled;
            rigidbody.isKinematic = true;
            rigidbody.velocity = Vector3.zero;
            Instantiate(explosion, transform.position, transform.rotation);
            body.SetActive(false);
            audio.volume = 0;
            SoundManager.Instance.PlaySound(deathSound);
            SpawnDeadBody();
        }
    }

    [ClientRpc]
    void Respawn()
    {
        transform.position = respawnTransform.position;
        transform.rotation = respawnTransform.rotation;
        currentWeapon = defaultWeapon;
        GetComponent<BoxCollider>().enabled = enabled;
        rigidbody.isKinematic = false;
        body.SetActive(true);
        isDead = false;
        respawnDalay = 5;
        _hp = hp;
    }


    [Client]
    void PlayerInitialization()
    {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();

        

        GameObject gobj = new GameObject();

        respawnTransform = gobj.transform;
        respawnTransform.position = transform.position;
        respawnTransform.rotation = transform.rotation;

        _hp = hp;

        if (isLocalPlayer)
        {
            transform.rotation = new Quaternion(transform.rotation.x, 0, transform.rotation.z, transform.rotation.w);
            rigidbody.constraints = RigidbodyConstraints.None;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            camera.SetActive(true);
            camera.transform.parent = null;
            transform.rotation = respawnTransform.rotation;
            audio.clip = moveSound;
            audio.volume = 0;
            audio.Play();
        }
        currentWeapon = defaultWeapon;
    }
    #endregion
}
