﻿using UnityEngine;
using Mirror;

public class DestroedPlayerBehaviour : NetworkBehaviour,IEntity
{
    public float hp = 2;
    [SerializeField] GameObject destroyParticles;
    [SerializeField] AudioClip destroySound;

    [ClientRpc]
    public void GetDamage(float damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            var destroyEffect = Instantiate(destroyParticles, transform.position, transform.rotation);
            NetworkServer.Spawn(destroyEffect);
            SoundManager.Instance.PlaySound(destroySound);
            Destroy(gameObject);
        }
    }
}
