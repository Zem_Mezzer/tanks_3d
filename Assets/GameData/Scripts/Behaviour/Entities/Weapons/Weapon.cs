using UnityEngine;

[CreateAssetMenu(fileName ="Weapon",menuName ="Create new weapon")]
public class Weapon:ScriptableObject
{
    public GameObject bulletPrefab;
    public float rechargeTime;
    public AudioClip shootSound;
    public GameObject fireEffect;
}
