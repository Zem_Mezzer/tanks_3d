using UnityEngine;

public class BombBehaviour : BulletBehaviour
{
    [SerializeField] float explosionRange;
    public override void GetDamage(float damage)
    {
        Collider[] objects = Physics.OverlapSphere(transform.position, explosionRange);
        Collider[] hit = Physics.OverlapSphere(transform.position, explosionRange);

        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].transform.root != transform)
            {
                if(objects[i].GetComponent<IEntity>()!=null)
                objects[i].gameObject.GetComponent<IEntity>().GetDamage(bulletDamage);
            }
        }

        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].gameObject.GetComponent<Rigidbody>() != null && objects[i].transform.root != transform)
            {
                hit[i].gameObject.GetComponent<Rigidbody>().AddExplosionForce(200, transform.position, 100, 1);
            }
        }
        base.GetDamage(damage);
    }
}
