﻿using UnityEngine;

public class LaserBulletBehaviour : BulletBehaviour
{
    [Mirror.ClientRpc]
    public override void GetDamage(float damage)
    {
        GetComponent<BoxCollider>().enabled = !enabled;
        rigidbody.isKinematic = true;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        SoundManager.Instance.PlaySound(explosionSound);
    }
}
