﻿using UnityEngine;

public class JumpBullet : BulletBehaviour
{
    [Mirror.ClientRpc]
    public override void GetDamage(float damage)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 0.7f);
        for(int i = 0; i < colliders.Length; i++)
        {
       
            if (colliders[i].gameObject.GetComponent<Rigidbody>() != null && colliders[i].transform.root != transform)
            {
                colliders[i].gameObject.GetComponent<Rigidbody>().AddExplosionForce(300, transform.position, 100, 10);
            }
        }
        base.GetDamage(damage);
    }
}
