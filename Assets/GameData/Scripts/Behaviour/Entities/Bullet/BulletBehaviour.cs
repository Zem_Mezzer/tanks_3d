﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class BulletBehaviour : NetworkBehaviour,IEntity
{
    [SerializeField] protected new Rigidbody rigidbody;
    [SerializeField] float speed;
    [SerializeField] GameObject destroyParticles;
    [SerializeField] protected AudioClip explosionSound;

    public float bulletDamage;

    [ClientRpc]
    public virtual void GetDamage(float damage)
    {
        var particles = Instantiate(destroyParticles,transform.position,transform.rotation);
        SoundManager.Instance.PlaySound(explosionSound);
        NetworkServer.Spawn(particles);
        NetworkServer.Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody.velocity = transform.forward * speed;
    }

    [ServerCallback]
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<IEntity>() != null)
        {
            collision.gameObject.GetComponent<IEntity>().GetDamage(bulletDamage);
        }
        Debug.Log(collision.gameObject.name);
        GetDamage(0);
    }
}
